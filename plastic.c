#include <stdio.h>
#include <math.h>

long int get_cc();
char validate(long int cc);

int main()
{
    long int ctest;
    ctest = get_cc();
    char result = validate(ctest);
    switch(result)
    {
        case'a':
            printf("AMEX\n");
            break;
        case'v':
            printf("VISA\n");
            break;
        case'm':
            printf("MASTERCARD\n");
            break;
        case'd':
            printf("DISCOVER\n");
            break;
        case'i':
            printf("INVALID\n");
            break;
    }
    
}

long int get_cc()
{
    long int ccnum;
    printf("Enter a credit card number: ");
    scanf("%li", &ccnum);
    return ccnum;
}

char validate(long int cc)
{
    long int oddnum = 0;
    long int evennum = 0;
    long int num;
    double startthree = pow(10.0, 14);
    double startelse = pow(10.0, 15);
    double isbigger = pow(10.0, 16);
    
    for(long int i = 0; i < 16; i++)
    {
        double exponent = pow(10.0, (double)i);
        num = (cc / (long int)exponent) % 10;
        
        if(i % 2 == 1)
        {
            num = num * 2;
            long int fdigit = (num / 10) % 10;
            long int sdigit = num % 10;
            oddnum = oddnum + fdigit + sdigit;
        }
        else
        {
            evennum = evennum + num;
        }
        
    }
    long int result = oddnum + evennum;
    
    if(result % 10 == 0)
    {
        if((cc/(long int)startthree)%10 == 3 && (cc/(long int)startelse)%10 == 0 )
        {
            return 'a';
        }
        else if((cc/(long int)startelse)%10 == 4 && (cc/(long int)isbigger)%10 == 0)
        {
            return 'v';
        }
        else if((cc/(long int)startelse)%10 == 5 && (cc/(long int)isbigger)%10 == 0)
        {
            return 'm';
        }
        else if((cc/(long int)startelse)%10 == 6 && (cc/(long int)isbigger)%10 == 0)
        {
            return 'd';
        }
        else
            return 'i';
    }
    else
        return 'i';
}
