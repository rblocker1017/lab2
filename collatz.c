#include <stdio.h>

int get_start();
int next_collatz(int num);

int main()
{
    int cnum = get_start();
    int count = 1;
    printf("Collatz Sequence: ");
    while(cnum != 1)
    {
        printf("%d, ", cnum);
        cnum = next_collatz(cnum);
        count++;
    }
    printf("%d\n", cnum);
    printf("Length: %d\n", count);
}

int get_start()
{
    int num;
    printf("Enter the starting number: ");
    scanf("%d", &num);
    while(num < 1)
    {
        printf("The number should be a positive integer.\n");
        printf("Enter the starting number: ");
        scanf("%d", &num);
    }
    return num;
}

int next_collatz(int num)
{
    if(num % 2 == 0)
    {
        num = num / 2;
    }
    else
    {
        num = (num * 3) + 1;
    }
    
    return num;
}